<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Tests\Unit\Service;

use SixBySix\Jira\ServiceDesk\Service\ServiceDeskClient;
use SixBySix\Jira\ServiceDesk\Tests\Unit\TestCase;


/**
 * Class ServiceDeskClientTest
 */
class ServiceDeskClientTest extends TestCase
{
    /** @var  */
    protected $sdClient;

    function setUp(): void
    {
        $this->sdClient = new ServiceDeskClient(self::BASE_URL);
    }

    /**
     * @test
     */
    public function itCanUseOauthCredentials()
    {
        $this->sdClient->oauth("oauthaccesstoken123");
        $httpConfig = $this->sdClient->getHttpClient()->getConfig();
        $this->assertEquals("Bearer oauthaccesstoken123", $httpConfig['headers']['Authorization']);
    }

    /**
     * @test
     */
    public function itCanUseBasicAuthCredentials()
    {
        $this->sdClient->basicAuth("jira-admin", "password123");
        $httpConfig = $this->sdClient->getHttpClient()->getConfig();
        $this->assertEquals(["jira-admin", "password123"], $httpConfig['auth']);
    }
}
