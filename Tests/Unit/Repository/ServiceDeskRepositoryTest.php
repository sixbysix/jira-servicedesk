<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Tests\Unit\Repository;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use SixBySix\Jira\ServiceDesk\Model\Contracts\ServiceDeskInterface;
use SixBySix\Jira\ServiceDesk\Model\ServiceDesk;
use SixBySix\Jira\ServiceDesk\Repository\ServiceDeskRepository;
use SixBySix\Jira\ServiceDesk\Service\ServiceDeskClient;
use SixBySix\Jira\ServiceDesk\Tests\Unit\TestCase;

class ServiceDeskRepositoryTest extends AbstractRepositoryTest
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = new ServiceDeskRepository($this->sdMock);
    }

    /**
     * @test
     */
    public function itCanList()
    {
        $responseMock = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock
            ->expects(self::once())
            ->method('getBody')
            ->willReturn(
<<<json
            {
    "_expands": [],
    "size": 3,
    "start": 3,
    "limit": 3,
    "isLastPage": false,
    "_links": {
        "base": "http://host:port/context/rest/servicedeskapi",
        "context": "context",
        "next": "http://host:port/context/rest/servicedeskapi/servicedesk?start=6&limit=3",
        "prev": "http://host:port/context/rest/servicedeskapi/servicedesk?start=0&limit=3"
    },
"values": [
{
"id": "10001",
"projectId": "11001",
"projectName": "IT Help Desk",
"projectKey": "ITH",
"_links": {
"self": "http://host:port/context/rest/servicedeskapi/servicedesk/10001"
}
},
{
    "id": "10002",
            "projectId": "11002",
            "projectName": "HR Self Serve Desk",
            "projectKey": "HR",
            "_links": {
                "self": "http://host:port/context/rest/servicedeskapi/servicedesk/10002"
            }
        },
{
    "id": "10003",
            "projectId": "11003",
            "projectName": "Foundation Leave",
            "projectKey": "FL",
            "_links": {
                "self": "http://host:port/context/rest/servicedeskapi/servicedesk/10003"
            }
        }
]
}
json);

        $this->httpMock
            ->expects(self::once())
            ->method('get')
            ->with('servicedesk')
            ->willReturn($responseMock);

        $list = $this->repo->list();
        $this->assertCount(3, $list->getIterator());
        $this->assertContainsOnlyInstancesOf(ServiceDeskInterface::class, $list->getIterator());
    }

    /**
     * @test
     */
    public function itCanGet()
    {
        $responseMock = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock
            ->expects(self::once())
            ->method('getBody')
            ->willReturn(
                <<<json
{
    "id": "10001",
    "projectId": "11001",
    "projectName": "IT Help Desk",
    "projectKey": "ITH",
    "_links": {
        "self": "http://host:port/context/rest/servicedeskapi/servicedesk/10001"
    }
}
json);

        $this->httpMock
            ->expects(self::once())
            ->method('get')
            ->with('servicedesk/10001')
            ->willReturn($responseMock);

        $serviceDesk = $this->repo->get(10001);

        $this->assertInstanceOf(ServiceDeskInterface::class, $serviceDesk);
        $this->assertEquals(10001, $serviceDesk->getId());
        $this->assertEquals(11001, $serviceDesk->getProjectId());
        $this->assertEquals(["self" => "http://host:port/context/rest/servicedeskapi/servicedesk/10001"], $serviceDesk->getLinks());
        $this->assertEquals("IT Help Desk", $serviceDesk->getProjectName());
        $this->assertEquals("ITH", $serviceDesk->getProjectKey());
    }
}
