<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Tests\Unit\Repository;

use GuzzleHttp\Psr7\Response;
use SixBySix\Jira\ServiceDesk\Model\Contracts\OrganizationInterface;
use SixBySix\Jira\ServiceDesk\Model\Contracts\UserInterface;
use SixBySix\Jira\ServiceDesk\Model\Organization;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\OrganizationRepositoryInterface;
use SixBySix\Jira\ServiceDesk\Repository\OrganizationRepository;

/**
 * Class OrganizationRepositoryTest
 *
 * @property OrganizationRepositoryInterface $repo
 */
class OrganizationRepositoryTest extends AbstractRepositoryTest
{

    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = new OrganizationRepository($this->sdMock);
    }

    /**
     * @test
     */
    public function itCanList(): void
    {
        $responseMock = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock
            ->expects(self::once())
            ->method('getBody')
            ->willReturn(
                <<<json
{
    "_expands": [],
    "size": 1,
    "start": 1,
    "limit": 1,
    "isLastPage": false,
    "_links": {
        "base": "http://host:port/context/rest/servicedeskapi",
        "context": "context",
        "next": "http://host:port/context/rest/servicedeskapi/organization?start=2&limit=1",
        "prev": "http://host:port/context/rest/servicedeskapi/organization?start=0&limit=1"
    },
    "values": [
        {
            "id": "1",
            "name": "Charlie Cakes Franchises",
            "_links": {
                "self": "http://host:port/context/rest/servicedeskapi/organization/1"
            }
        }
    ]
}
json);

        $this->httpMock
            ->expects(self::once())
            ->method('get')
            ->with('organization')
            ->willReturn($responseMock);

        $list = $this->repo->list();
        $this->assertCount(1, $list->getIterator());
        $this->assertContainsOnlyInstancesOf(OrganizationInterface::class, $list->getIterator());
    }

    /**
     * @test
     */
    public function itCanGet()
    {
        $responseMock = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock
            ->expects(self::once())
            ->method('getBody')
            ->willReturn(
                <<<json
{
    "id": "1",
    "name": "Charlie Cakes Franchises",
    "_links": {
        "self": "http://host:port/context/rest/servicedeskapi/organization/1"
    }
}
json);

        $this->httpMock
            ->expects(self::once())
            ->method('get')
            ->with('organization/1')
            ->willReturn($responseMock);

        /** @var OrganizationInterface $org */
        $org = $this->repo->get(1);

        $this->assertInstanceOf(OrganizationInterface::class, $org);
        $this->assertEquals(1, $org->getId());
        $this->assertEquals("Charlie Cakes Franchises", $org->getName());
    }

    /**
     * @test
     */
    public function itCanCreate()
    {
        $responseMock = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock
            ->expects(self::once())
            ->method('getBody')
            ->willReturn(
                <<<json
{
    "id": "1",
    "name": "Charlie Cakes Franchises",
    "_links": {
        "self": "http://host:port/context/rest/servicedeskapi/organization/1"
    }
}
json);

        $orgName = "Charlie Cakes Franchises";

        $this->httpMock
            ->expects(self::once())
            ->method('post')
            ->with('organization', [
                'json' => [
                    'name' => $orgName,
                ]
            ])
            ->willReturn($responseMock);

        /** @var OrganizationInterface $org */
        $org = $this->repo->create($orgName);

        $this->assertInstanceOf(OrganizationInterface::class, $org);
        $this->assertEquals(1, $org->getId());
        $this->assertEquals("Charlie Cakes Franchises", $org->getName());
    }

    /**
     * @test
     */
    public function itCanDelete()
    {
        $responseMock = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $org = new Organization();
        $org->setId(1);
        $org->setName("SixBySix");

        $responseMock
            ->expects(self::once())
            ->method('getStatusCode')
            ->willReturn(200);

        $this->httpMock
            ->expects(self::once())
            ->method('delete')
            ->with('organization/1')
            ->willReturn($responseMock);

        $orgResponse = $this->repo->delete($org);

        $this->assertInstanceOf(OrganizationInterface::class, $orgResponse);
        $this->assertEquals(1, $org->getId());
        $this->assertEquals("SixBySix", $org->getName());
    }

    /**
     * @test
     */
    public function itCanGetOrganisationUsers()
    {
        $responseMock = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock
            ->expects(self::once())
            ->method('getBody')
            ->willReturn(
                <<<json
{
    "_expands": [],
    "size": 1,
    "start": 1,
    "limit": 1,
    "isLastPage": false,
    "_links": {
        "base": "http://host:port/context/rest/servicedeskapi",
        "context": "context",
        "next": "http://host:port/context/rest/servicedeskapi/organization/1/user?start=2&limit=1",
        "prev": "http://host:port/context/rest/servicedeskapi/organization/1/user?start=0&limit=1"
    },
    "values": [
        {
            "name": "fred",
            "key": "fred",
            "emailAddress": "fred@example.com",
            "displayName": "Fred F. User",
            "active": true,
            "timeZone": "Australia/Sydney",
            "_links": {
                "jiraRest": "http://www.example.com/jira/rest/api/2/user?username=fred",
                "avatarUrls": {
                    "48x48": "http://www.example.com/jira/secure/useravatar?size=large&ownerId=fred",
                    "24x24": "http://www.example.com/jira/secure/useravatar?size=small&ownerId=fred",
                    "16x16": "http://www.example.com/jira/secure/useravatar?size=xsmall&ownerId=fred",
                    "32x32": "http://www.example.com/jira/secure/useravatar?size=medium&ownerId=fred"
                },
                "self": "http://www.example.com/jira/rest/api/2/user?username=fred"
            }
        }
    ]
}
json);

        $this->httpMock
            ->expects(self::once())
            ->method('get')
            ->with('organization/1/user')
            ->willReturn($responseMock);

        $org = new Organization();
        $org->setId(1);
        $org->setName("SixBySix");

        $list = $this->repo->getUsers($org);
        $this->assertCount(1, $list->getIterator());
        $this->assertContainsOnlyInstancesOf(UserInterface::class, $list->getIterator());
    }

    /**
     * @test
     */
    public function itCanAddOrganizationUsers()
    {
        $responseMock = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $org = new Organization();
        $org->setId(1);
        $org->setName("SixBySix");

        $responseMock
            ->expects(self::any())
            ->method('getStatusCode')
            ->willReturn(204);

        $responseMock
            ->expects(self::once())
            ->method('getBody')
            ->willReturn(<<<json
{
    "_expands": [],
    "size": 3,
    "start": 1,
    "limit": 3,
    "isLastPage": true,
    "_links": {
        "base": "http://host:port/context/rest/servicedeskapi",
        "context": "context",
        "next": "http://host:port/context/rest/servicedeskapi/organization/1/user?start=4&limit=2",
        "prev": "http://host:port/context/rest/servicedeskapi/organization/1/user?start=0&limit=2"
    },
    "values": [
        {
            "name": "joe",
            "key": "joe",
            "emailAddress": "joe@example.com",
            "displayName": "Joe J. User",
            "active": true,
            "timeZone": "Australia/Sydney",
            "_links": {
                "jiraRest": "http://www.example.com/jira/rest/api/2/user?username=joe",
                "avatarUrls": {
                    "48x48": "http://www.example.com/jira/secure/useravatar?size=large&ownerId=joe",
                    "24x24": "http://www.example.com/jira/secure/useravatar?size=small&ownerId=joe",
                    "16x16": "http://www.example.com/jira/secure/useravatar?size=xsmall&ownerId=joe",
                    "32x32": "http://www.example.com/jira/secure/useravatar?size=medium&ownerId=joe"
                },
                "self": "http://www.example.com/jira/rest/api/2/user?username=joe"
            }
        },
        {
            "name": "john",
            "key": "john",
            "emailAddress": "john@example.com",
            "displayName": "John J. User",
            "active": true,
            "timeZone": "Australia/Sydney",
            "_links": {
                "jiraRest": "http://www.example.com/jira/rest/api/2/user?username=john",
                "avatarUrls": {
                    "48x48": "http://www.example.com/jira/secure/useravatar?size=large&ownerId=john",
                    "24x24": "http://www.example.com/jira/secure/useravatar?size=small&ownerId=john",
                    "16x16": "http://www.example.com/jira/secure/useravatar?size=xsmall&ownerId=john",
                    "32x32": "http://www.example.com/jira/secure/useravatar?size=medium&ownerId=john"
                },
                "self": "http://www.example.com/jira/rest/api/2/user?username=john"
            }
        },
        {
            "name": "josh",
            "key": "josh",
            "emailAddress": "josh@example.com",
            "displayName": "Josh J. User",
            "active": true,
            "timeZone": "Australia/Sydney",
            "_links": {
                "jiraRest": "http://www.example.com/jira/rest/api/2/user?username=josh",
                "avatarUrls": {
                    "48x48": "http://www.example.com/jira/secure/useravatar?size=large&ownerId=josh",
                    "24x24": "http://www.example.com/jira/secure/useravatar?size=small&ownerId=josh",
                    "16x16": "http://www.example.com/jira/secure/useravatar?size=xsmall&ownerId=josh",
                    "32x32": "http://www.example.com/jira/secure/useravatar?size=medium&ownerId=josh"
                },
                "self": "http://www.example.com/jira/rest/api/2/user?username=josh"
            }
        }
    ]
}
json);

        $this->httpMock
            ->expects(self::once())
            ->method('post')
            ->with('organization/1/user', [
                'json' => [
                    'usernames' => ['joe', 'john', 'josh']
                ]
            ])
            ->willReturn($responseMock);

        $users = $this->repo->addUsers($org, ['joe', 'john', 'josh']);
        $this->assertCount(3, $users->getIterator());
        $this->assertContainsOnlyInstancesOf(UserInterface::class, $users->getIterator());
    }

    /**
     * @test
     */
    public function itCanDeleteOrganizationUsers()
    {
        $responseMock = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $org = new Organization();
        $org->setId(1);
        $org->setName("SixBySix");

        $responseMock
            ->expects(self::once())
            ->method('getStatusCode')
            ->willReturn(204);

        $this->httpMock
            ->expects(self::once())
            ->method('delete')
            ->with('organization/1/user')
            ->willReturn($responseMock);

        $result = $this->repo->deleteUsers($org, ['joe']);
        $this->assertTrue($result);
    }
}
