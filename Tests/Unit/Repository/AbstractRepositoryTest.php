<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Tests\Unit\Repository;

use GuzzleHttp\Client;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\RepositoryInterface;
use SixBySix\Jira\ServiceDesk\Service\ServiceDeskClient;

class AbstractRepositoryTest extends \SixBySix\Jira\ServiceDesk\Tests\Unit\TestCase
{
    /** @var Client */
    protected $httpMock;
    /** @var RepositoryInterface */
    protected $repo;
    /** @var ServiceDeskClient */
    protected $sdMock;

    protected function setUp(): void
    {
        $this->httpMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->sdMock = $this->getMockBuilder(ServiceDeskClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->sdMock->method('getHttpClient')->willReturn($this->httpMock);

        parent::setUp();
    }
}
