<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Tests\Unit;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    const BASE_URL = 'https://sixbysix.atlassian.net/rest/servicedeskapi/';
}
