<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Tests\Unit\Model;

use SixBySix\Jira\ServiceDesk\Model\Organization;
use SixBySix\Jira\ServiceDesk\Tests\Unit\TestCase;

class OrganizationTest extends TestCase
{
    /** @var Organization */
    protected $model;

    public function setUp(): void
    {
        parent::setUp();

        $this->model = new Organization();
        $this->model
            ->setId(123)
            ->setName("SixBySix");
    }

    /**
     * @test
     */
    public function getters()
    {
        $this->assertEquals(123, $this->model->getId());
        $this->assertEquals('SixBySix', $this->model->getName());
    }
}
