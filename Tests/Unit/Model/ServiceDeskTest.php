<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Tests\Unit\Model;

use SixBySix\Jira\ServiceDesk\Model\ServiceDesk;
use PHPUnit\Framework\TestCase;

class ServiceDeskTest extends TestCase
{
    /** @var ServiceDesk */
    protected $model;

    public function setUp(): void
    {
        parent::setUp();

        $this->model = new ServiceDesk();
        $this->model
            ->setId("12345")
            ->setProjectKey('SBS')
            ->setProjectName('SixBySix')
            ->setProjectId("1233")
            ->setLinks([
                'self' => "http://host:port/context/rest/servicedeskapi/servicedesk/10001",
            ]);
    }

    /**
     * @test
     */
    public function getters()
    {
        $this->assertEquals(12345, $this->model->getId());
        $this->assertEquals('SBS', $this->model->getProjectKey());
        $this->assertEquals('SixBySix', $this->model->getProjectName());
        $this->assertEquals(1233, $this->model->getProjectId());
        $this->assertEquals([
        'self' => "http://host:port/context/rest/servicedeskapi/servicedesk/10001",
        ], $this->model->getLinks());
    }
}
