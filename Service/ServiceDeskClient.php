<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Service;

use GuzzleHttp\Client;
use SixBySix\Jira\ServiceDesk\Service\Contracts\ServiceDeskClientInterface;

/**
 * Class ServiceDeskClient
 */
class ServiceDeskClient implements ServiceDeskClientInterface
{
    const AUTH_OAUTH = 'oauth';
    const AUTH_BASIC = 'basic';

    protected Client $httpClient;
    protected string $baseUrl;
    protected string $authType;
    protected string $apiUrl;
    protected string $oauthAccessToken;
    protected string $basicUsername;
    protected string $basePassword;

    /**
     * ServiceDeskClient constructor.
     * @param string $baseUrl
     */
    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Use OAuth
     * @see https://developer.atlassian.com/server/jira/platform/oauth/
     * @param string $accessToken
     */
    public function oauth(string $accessToken)
    {
        $this->authType = self::AUTH_OAUTH;
        $this->oauthAccessToken = $accessToken;
    }

    /**
     * Use basic auth
     * @see https://developer.atlassian.com/server/jira/platform/basic-authentication/
     * @param string $username
     * @param string $password
     */
    public function basicAuth(string $username, string $password)
    {
        $this->authType = self::AUTH_BASIC;
        $this->basicUsername = $username;
        $this->basePassword = $password;
    }

    /**
     * @return Client
     */
    public function getHttpClient(): Client
    {
        return $this->httpClient ?? $this->initClient();
    }

    protected function initClient(): Client
    {
        $config = [
            'headers' => [
                'Content-Type' => "application/json",
            ]
        ];

        if ($this->authType == self::AUTH_BASIC) {
            $config['auth'] = [
                $this->basicUsername,
                $this->basePassword,
            ];
        } else {
            $config['headers']['Authorization'] = 'Bearer ' . $this->oauthAccessToken;
        }

        $this->httpClient = new Client($config);
        return $this->httpClient;
    }
}
