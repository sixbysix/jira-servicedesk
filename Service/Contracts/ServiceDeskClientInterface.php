<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Service\Contracts;

use GuzzleHttp\Client;

/**
 * Interface ServiceDeskClientInterface
 * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/
 */
interface ServiceDeskClientInterface
{
    /**
     * @return Client
     */
    public function getHttpClient(): Client;
}
