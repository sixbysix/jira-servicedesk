<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Repository;

use SixBySix\Jira\ServiceDesk\Model\ServiceDesk;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\Api\PaginatedResponseInterface;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\ServiceDeskRepositoryInterface;
use SixBySix\Jira\ServiceDesk\Model\Contracts\ServiceDeskInterface;
use SixBySix\Jira\ServiceDesk\Service\Contracts\ServiceDeskClientInterface;

/**
 * Class ServiceDeskRepository
 */
class ServiceDeskRepository extends AbstractApiRepository implements ServiceDeskRepositoryInterface
{
    const API_LIST_ENDPOINT = "servicedesk";

    /**
     * @inheritDoc
     */
    public function list(): PaginatedResponseInterface
    {
        $response = $this->getApi()->get('servicedesk');
        return $this->paginatedResponse($response, $this->getHydrator());
    }

    /**
     * @inheritDoc
     */
    public function get(int $id): ServiceDeskInterface
    {
        $response = $this->getApi()->get("servicedesk/$id");
        return $this->response($response, $this->getHydrator())->getItem();
    }

    /**
     * @inheritDoc
     */
    public function attachTemporaryFile(int $serviceDeskId): array
    {
        // TODO: Implement attachTemporaryFile() method.
    }

    private function getHydrator(): \Closure
    {
        return (function (array $json): ServiceDeskInterface {
            $sd = new ServiceDesk();
            $sd->setId($json['id']);
            $sd->setProjectId($json['projectId']);
            $sd->setProjectKey($json['projectKey']);
            $sd->setProjectName($json['projectName']);
            $sd->setLinks($json['_links']);
            return $sd;
        });
    }
}
