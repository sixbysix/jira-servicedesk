<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Repository\Api;

use GuzzleHttp\Client;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\Api\PaginatedResponseInterface;

class PaginatedResponse extends Response implements PaginatedResponseInterface
{
    protected Client $httpClient;
    protected array $items = [];
    protected bool $haveFetchedAllItems = false;
    protected string $nextPageUrl;

    public function __construct(
        \GuzzleHttp\Psr7\Response $response,
        Client $httpClient,
        \Closure $hydrator
    ) {
        $this->httpClient = $httpClient;
        parent::__construct($response, $hydrator);
    }

    public function getIterator(): \Iterator
    {
        return (function () {
            foreach($this->items as $val) {
                yield ($this->hydrator)($val);
            }
        })();
    }

    protected function parseResponse(array $response): void
    {
        if ((bool) $response['isLastPage']) {
            $this->haveFetchedAllItems = true;
        }

        $this->nextPageUrl = $response['_links']['next'];
        $this->items = $response['values'];
    }
}
