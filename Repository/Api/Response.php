<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Repository\Api;

use SixBySix\Jira\ServiceDesk\Model\Contracts\ModelInterface;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\Api\ResponseInterface;

class Response implements ResponseInterface
{
    protected \Closure $hydrator;

    protected ModelInterface $item;

    public function __construct(\GuzzleHttp\Psr7\Response $response, \Closure $hydrator)
    {
        $this->hydrator = $hydrator;

        $jsonResponse = json_decode((string) $response->getBody(), true);
        $this->parseResponse($jsonResponse);
    }

    public function getItem(): ModelInterface
    {
        return $this->item;
    }

    protected function parseResponse(array $response)
    {
        $this->item = ($this->hydrator)($response);
    }
}
