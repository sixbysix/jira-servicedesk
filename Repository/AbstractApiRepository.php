<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Repository;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use SixBySix\Jira\ServiceDesk\Repository\Api\PaginatedResponse;
use SixBySix\Jira\ServiceDesk\Repository\Api\Response;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\Api\PaginatedResponseInterface;
use SixBySix\Jira\ServiceDesk\Service\Contracts\ServiceDeskClientInterface;

class AbstractApiRepository implements Contracts\ApiRepositoryInterface
{
    protected ServiceDeskClientInterface $serviceDeskClient;
    protected string $hydratedClassName;

    public function __construct(ServiceDeskClientInterface $serviceDeskClient)
    {
        $this->serviceDeskClient = $serviceDeskClient;
    }

    protected function getApi(): Client
    {
        return $this->serviceDeskClient->getHttpClient();
    }

    public function paginatedResponse(ResponseInterface $response, \Closure $hydrator): PaginatedResponseInterface
    {
        return new PaginatedResponse($response, $this->getApi(), $hydrator);
    }

    public function response(ResponseInterface $response, \Closure $hydrator): \SixBySix\Jira\ServiceDesk\Repository\Contracts\Api\ResponseInterface
    {
        return new Response($response, $hydrator);
    }
}
