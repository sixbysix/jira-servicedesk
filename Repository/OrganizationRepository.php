<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Repository;

use SixBySix\Jira\ServiceDesk\Model\Contracts\OrganizationInterface;
use SixBySix\Jira\ServiceDesk\Model\Contracts\UserInterface;
use SixBySix\Jira\ServiceDesk\Model\Organization;
use SixBySix\Jira\ServiceDesk\Model\User;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\Api\PaginatedResponseInterface;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\Api\ResponseInterface;

class OrganizationRepository extends AbstractApiRepository implements Contracts\OrganizationRepositoryInterface
{
    public function list(): PaginatedResponseInterface
    {
        return $this->paginatedResponse($this->getApi()->get('organization'), $this->getHydrator());
    }

    public function get(int $id): OrganizationInterface
    {
        return $this->response(
            $this->getApi()->get("organization/$id"),
            $this->getHydrator()
        )->getItem();
    }

    public function create(string $organizationName): OrganizationInterface
    {
        return $this->response(
            $this->getApi()->post(
                "organization", [
                    'json' => [
                        'name' => $organizationName
                    ]
                ]
            ),
            $this->getHydrator()
        )->getItem();
    }

    public function delete(OrganizationInterface $organization): OrganizationInterface
    {
        $response = $this->getApi()->delete("organization/" . $organization->getId());
        switch ($response->getStatusCode()) {
            case 200:
                return $organization;
            default:
                throw new \Exception();
        }
    }

    public function getUsers(OrganizationInterface $organization): PaginatedResponseInterface
    {
        return $this->paginatedResponse(
            $this->getApi()->get("organization/{$organization->getId()}/user"),
            $this->getUserHydrator()
        );
    }

    public function addUsers(OrganizationInterface $organization, array $usernames): PaginatedResponseInterface
    {
        return $this->paginatedResponse(
            $this->getApi()->post("organization/{$organization->getId()}/user", [
                'json' => [
                    'usernames' => $usernames,
                ]
            ]),
            $this->getUserHydrator()
        );
    }

    public function deleteUsers(OrganizationInterface $organization, array $usernames): bool
    {
        $response = $this->getApi()->delete("organization/{$organization->getId()}/user", [
            'json' => [
                'usernames' => $usernames,
            ]
        ]);

        switch ($response->getStatusCode()) {
            case 204:
                return true;
            default:
                throw new \Exception();
        }
    }

    protected function getHydrator(): \Closure
    {
        return (function (array $json): OrganizationInterface {
            $org = new Organization();
            $org->setId((int) $json['id']);
            $org->setName($json['name']);
            return $org;
        });
    }

    protected function getUserHydrator(): \Closure
    {
        return (function (array $json): UserInterface {
            $user = new User();
            return $user;
        });
    }
}
