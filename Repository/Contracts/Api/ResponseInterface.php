<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Repository\Contracts\Api;

use SixBySix\Jira\ServiceDesk\Model\Contracts\ModelInterface;

interface ResponseInterface
{
    public function getItem(): ModelInterface;
}
