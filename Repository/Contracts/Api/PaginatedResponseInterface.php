<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Repository\Contracts\Api;

interface PaginatedResponseInterface extends ResponseInterface, \IteratorAggregate
{

}
