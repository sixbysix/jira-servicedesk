<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Repository\Contracts;

use SixBySix\Jira\ServiceDesk\Model\Contracts\ServiceDeskInterface;

/**
 * Interface ServiceDeskRepositoryInterface
 * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/servicedesk
 */
interface ServiceDeskRepositoryInterface extends ApiRepositoryInterface
{
    /**
     * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/servicedesk-getServiceDesks
     */
    public function list();

    /**
     * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/servicedesk-getServiceDeskById
     * @param int $id
     * @return ServiceDeskInterface
     */
    public function get(int $id): ServiceDeskInterface;

    /**
     * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/servicedesk/{serviceDeskId}/attachTemporaryFile-attachTemporaryFile
     * @param int $serviceDeskId
     * @return array
     */
    public function attachTemporaryFile(int $serviceDeskId): array;
}
