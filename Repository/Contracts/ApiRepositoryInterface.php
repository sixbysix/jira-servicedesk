<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Repository\Contracts;

use SixBySix\Jira\ServiceDesk\Service\Contracts\ServiceDeskClientInterface;

/**
 * Interface ApiRepositoryInterface
 */
interface ApiRepositoryInterface extends RepositoryInterface
{
    public function __construct(ServiceDeskClientInterface $serviceDeskClient);
}
