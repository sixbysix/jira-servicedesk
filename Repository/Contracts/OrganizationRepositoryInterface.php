<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Repository\Contracts;

use SixBySix\Jira\ServiceDesk\Model\Contracts\OrganizationInterface;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\Api\PaginatedResponseInterface;
use SixBySix\Jira\ServiceDesk\Repository\Contracts\Api\ResponseInterface;

interface OrganizationRepositoryInterface extends ApiRepositoryInterface
{
    /**
     * @return PaginatedResponseInterface
     * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/organization-getOrganizations
     */
    public function list(): PaginatedResponseInterface;

    /**
     * @param int $id
     * @return ResponseInterface
     * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/organization-getOrganization
     */
    public function get(int $id): OrganizationInterface;

    /**
     * @param string $organizationName
     * @return OrganizationInterface
     * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/organization-createOrganization
     */
    public function create(string $organizationName): OrganizationInterface;

    /**
     * @param OrganizationInterface $organization
     * @return OrganizationInterface
     * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/organization-deleteOrganization
     */
    public function delete(OrganizationInterface $organization): OrganizationInterface;

    /**
     * @param OrganizationInterface $organization
     * @return PaginatedResponseInterface
     * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/organization-getUsersInOrganization
     */
    public function getUsers(OrganizationInterface $organization): PaginatedResponseInterface;

    /**
     * @param OrganizationInterface $organization
     * @param string[] $usernames
     * @return PaginatedResponseInterface
     * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/organization-addUsersToOrganization
     */
    public function addUsers(OrganizationInterface $organization, array $usernames): PaginatedResponseInterface;

    /**
     * @param OrganizationInterface $organization
     * @param array $usernames
     * @return bool
     * @see https://docs.atlassian.com/jira-servicedesk/REST/3.6.2/#servicedeskapi/organization-removeUsersFromOrganization
     */
    public function deleteUsers(OrganizationInterface $organization, array $usernames): bool;
}
