<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Model;

use SixBySix\Jira\ServiceDesk\Model\Contracts\UserInterface;

class User extends Contracts\AbstractModel implements UserInterface
{
    protected string $name;
    protected string $key;
    protected string $emailAddress;
    protected string $displayName;
    protected bool $active;
    protected string $timeZone;
    protected array $avatars;
    protected string $jiraRestUrl;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return User
     */
    public function setKey(string $key): User
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     * @return User
     */
    public function setEmailAddress(string $emailAddress): User
    {
        $this->emailAddress = $emailAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @param string $displayName
     * @return User
     */
    public function setDisplayName(string $displayName): User
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return User
     */
    public function setActive(bool $active): User
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimeZone(): string
    {
        return $this->timeZone;
    }

    /**
     * @param string $timeZone
     * @return User
     */
    public function setTimeZone(string $timeZone): User
    {
        $this->timeZone = $timeZone;
        return $this;
    }

    /**
     * @return array
     */
    public function getAvatars(): array
    {
        return $this->avatars;
    }

    /**
     * @param array $avatars
     * @return User
     */
    public function setAvatars(array $avatars): User
    {
        $this->avatars = $avatars;
        return $this;
    }

    /**
     * @return string
     */
    public function getJiraRestUrl(): string
    {
        return $this->jiraRestUrl;
    }

    /**
     * @param string $jiraRestUrl
     * @return User
     */
    public function setJiraRestUrl(string $jiraRestUrl): User
    {
        $this->jiraRestUrl = $jiraRestUrl;
        return $this;
    }
}
