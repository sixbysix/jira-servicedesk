<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Model\Contracts;

interface ServiceDeskInterface extends ModelInterface
{
    public function getId(): int;
    public function setId(string $id): ServiceDeskInterface;
    public function getProjectId(): int;
    public function setProjectId(string $projectId): ServiceDeskInterface;
    public function getProjectName(): string;
    public function setProjectName(string $projectName): ServiceDeskInterface;
    public function getProjectKey(): string;
    public function setProjectKey(string $projectKey): ServiceDeskInterface;
    public function getLinks(): array;
    public function setLinks(array $links): ServiceDeskInterface;
}
