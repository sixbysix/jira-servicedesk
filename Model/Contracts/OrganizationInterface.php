<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Model\Contracts;

interface OrganizationInterface extends ModelInterface
{
    public function getId(): int;
    public function setId(int $id): OrganizationInterface;
    public function getName(): string;
    public function setName(string $name): OrganizationInterface;
}
