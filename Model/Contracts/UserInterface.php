<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Model\Contracts;

/**
 * Interface UserInterface
 */
interface UserInterface extends ModelInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return UserInterface
     */
    public function setName(string $name): UserInterface;

    /**
     * @return string
     */
    public function getKey(): string;

    /**
     * @param string $key
     * @return UserInterface
     */
    public function setKey(string $key): UserInterface;

    /**
     * @return string
     */
    public function getEmailAddress(): string;

    /**
     * @param string $emailAddress
     * @return UserInterface
     */
    public function setEmailAddress(string $emailAddress): UserInterface;

    /**
     * @return string
     */
    public function getDisplayName(): string;

    /**
     * @param string $displayName
     * @return UserInterface
     */
    public function setDisplayName(string $displayName): UserInterface;

    /**
     * @return bool
     */
    public function isActive(): bool;

    /**
     * @param bool $active
     * @return UserInterface
     */
    public function setActive(bool $active): UserInterface;

    /**
     * @return string
     */
    public function getTimeZone(): string;

    /**
     * @param string $timeZone
     * @return UserInterface
     */
    public function setTimeZone(string $timeZone): UserInterface;

    /**
     * @return array
     */
    public function getAvatars(): array;

    /**
     * @param array $avatars
     * @return UserInterface
     */
    public function setAvatars(array $avatars): UserInterface;

    /**
     * @return string
     */
    public function getJiraRestUrl(): string;

    /**
     * @param string $jiraRestUrl
     * @return UserInterface
     */
    public function setJiraRestUrl(string $jiraRestUrl): UserInterface;
}
