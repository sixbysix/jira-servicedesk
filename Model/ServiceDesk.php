<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Model;

use SixBySix\Jira\ServiceDesk\Model\Contracts\AbstractModel;
use SixBySix\Jira\ServiceDesk\Model\Contracts\ServiceDeskInterface;

/**
 * Class ServiceDesk
 */
class ServiceDesk extends AbstractModel implements ServiceDeskInterface
{
    protected int $id;
    protected int $projectId;
    protected string $projectName;
    protected string $projectKey;
    protected array $links;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return ServiceDesk
     */
    public function setId(string $id): ServiceDesk
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getProjectId(): int
    {
        return $this->projectId;
    }

    /**
     * @param string $projectId
     * @return ServiceDesk
     */
    public function setProjectId(string $projectId): ServiceDesk
    {
        $this->projectId = (int) $projectId;
        return $this;
    }

    /**
     * @return string
     */
    public function getProjectName(): string
    {
        return $this->projectName;
    }

    /**
     * @param string $projectName
     * @return ServiceDesk
     */
    public function setProjectName(string $projectName): ServiceDesk
    {
        $this->projectName = $projectName;
        return $this;
    }

    /**
     * @return string
     */
    public function getProjectKey(): string
    {
        return $this->projectKey;
    }

    /**
     * @param string $projectKey
     * @return ServiceDesk
     */
    public function setProjectKey(string $projectKey): ServiceDesk
    {
        $this->projectKey = $projectKey;
        return $this;
    }

    /**
     * @return array
     */
    public function getLinks(): array
    {
        return $this->links;
    }

    /**
     * @param array $links
     * @return ServiceDesk
     */
    public function setLinks(array $links): ServiceDesk
    {
        $this->links = $links;
        return $this;
    }
}
