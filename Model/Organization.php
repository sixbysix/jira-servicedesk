<?php
declare(strict_types=1);

namespace SixBySix\Jira\ServiceDesk\Model;

use SixBySix\Jira\ServiceDesk\Model\Contracts\AbstractModel;
use SixBySix\Jira\ServiceDesk\Model\Contracts\OrganizationInterface;

class Organization extends AbstractModel implements OrganizationInterface
{
    protected int $id;
    protected string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): OrganizationInterface
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): OrganizationInterface
    {
        $this->name = $name;
        return $this;
    }

}
